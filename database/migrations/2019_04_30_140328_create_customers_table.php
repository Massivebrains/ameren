<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('account_id')->nullable();
            $table->string('client_key')->nullable();
            $table->string('account_external_id')->nullable();
            $table->string('account_active_date')->nullable();
            $table->string('account_type')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_postal_code')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_street')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_postal_code')->nullable();
            $table->string('shipping_street')->nullable();
            $table->string('business_type')->nullable();
            $table->string('customer_class')->nullable();
            $table->string('electric_premise_number')->nullable();
            $table->string('electric_annual_bill')->nullable();
            $table->string('electric_annual_usage')->nullable();
            $table->string('electric_meter_number')->nullable();
            $table->string('electric_rate_code')->nullable();
            $table->string('electric_utility_name')->nullable();
            $table->string('flat')->nullable();
            $table->string('gas_premise_number')->nullable();
            $table->string('gas_annual_bill')->nullable();
            $table->string('gas_annual_usage')->nullable();
            $table->string('gas_utility_name')->nullable();
            $table->string('gas_meter_number')->nullable();
            $table->string('gas_service_class')->nullable();
            $table->string('gas_rate_code')->nullable();
            $table->string('gas_utility_id')->nullable();
            $table->string('water_premise_number')->nullable();
            $table->string('water_annual_bill')->nullable();
            $table->string('water_annual_usage')->nullable();
            $table->string('water_utility_name')->nullable();
            $table->string('water_meter_number')->nullable();
            $table->string('water_service_class')->nullable();
            $table->string('water_rate_code')->nullable();
            $table->string('housing_type')->nullable();
            $table->string('low_income_referral')->nullable();
            $table->string('utility_id')->nullable();
            $table->string('program')->nullable();
            $table->string('client_account_external_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('external_id')->nullable();
            $table->string('record_type')->nullable();
            $table->string('energy_type_class')->nullable();
            $table->string('energy_type_class_label')->nullable();
            $table->string('water_heat_source')->nullable();
            $table->string('parent_id')->nullable();
            $table->string('square_footage')->nullable();
            $table->string('custom_data_value_1')->nullable();
            $table->string('custom_data_value_2')->nullable();
            $table->string('custom_data_value_3')->nullable();
            $table->string('custom_data_value_4')->nullable();
            $table->string('custom_data_value_5')->nullable();
            $table->string('traits_disability')->nullable();
            $table->string('traits_language')->nullable();
            $table->string('traits_site')->nullable();
            $table->string('traits')->nullable();
            $table->string('is_active')->nullable();
            $table->string('parent_account_name')->nullable();
            $table->string('master_file_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
