<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function(){

	Route::get('/token', 'AccessTokenController@index');

	Route::group(['prefix' => 'customers'], function(){

		Route::post('search', 'CustomersController@search');
	});
});