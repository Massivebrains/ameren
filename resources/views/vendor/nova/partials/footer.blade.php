<p class="mt-8 text-center text-xs text-80">
    <a href="{{url('/')}}" class="text-primary dim no-underline">Ameren Client</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Ameren - By <a href="mailto:vadeshayo@gmail.com">Olaiya Segun</a>.
    <span class="px-1">&middot;</span>
    v{{ Laravel\Nova\Nova::version() }}
</p>
