<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BifrostApi;
use App\AccessToken;
use Carbon\Carbon;

class AccessTokenController extends Controller
{
	public function index()
	{

		$accessToken = AccessToken::getToken();

		if(!$accessToken)
			return response()->json(['status' => false, 'data' => 'Token could not be refreshed from Bifrost']);

		return response()->json(['status' => true, 'data' => $accessToken]);
	}
}
