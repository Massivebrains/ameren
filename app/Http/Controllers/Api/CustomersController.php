<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BifrostApi;
use App\Customer;

class CustomersController extends Controller
{
	public function search(Request $request)
	{	
		$customers = Customer::searchDB();

		if($customers->count() > 0)
			return response()->json(['status' => true, 'data' => $customers]);

		$customers = Customer::searchDB();

		if($customers->count() > 0)
			return response()->json(['status' => true, 'data' => $customers]);

		$customers = $this->pullCustomersFromBifrost();

		if($customers){

			return response()->json(['status' => true, 'data' => $customers]);
		}

		return response()->json(['status' => true, 'data' => 'No Customers match your search Query.']);
	} 

	private function pullCustomersFromBifrost()
	{
		$response = Customer::searchBifrost();

		if($response['status'] == false) return null;

		$data = $response['data'];

		if(count($data) < 1) return null;

		$customers = [];

		foreach($data as $row){

			$bifrostCustomer = (object)$row;

			if(isset($bifrostCustomer->Id) == false || $bifrostCustomer->Id == null)
				continue;

			$mappedCustomer = Customer::mapBifrostCustomerToDBCustomer($bifrostCustomer);

			$customers[] = Customer::updateOrCreate(['account_id' => $bifrostCustomer->Id], $mappedCustomer);
		}

		return $customers;
	}  
}
