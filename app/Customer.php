<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BifrostApi;

class Customer extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];

	public static function searchDB()
	{
		$customers = Customer::where(['client_key' => config('app.client_key')]);

 		if(request('account_number')) 
 			$customers->orWhere(['account_id' => request('account_id')]);

 		if(request('customer_name')) 
 			$customers->orWhere(['name' => request('customer_name')]);

 		if(request('meter_number')) 
 			$customers->orWhere(['meter_number' => request('meter_number')]);

 		if(request('premise_number')) 
 			$customers->orWhere(['premise_number' => request('premise_number')]);

 		if(request('phone_number')) 
 			$customers->orWhere(['phone_number' => request('phone_number')]);

 		if(request('house_number')) 
 			$customers->orWhere(['house_number' => request('house_number')]);

 		if(request('street_name')) 
 			$customers->orWhere(['street_name' => request('street_name')]);

 		if(request('city_name')) 
 			$customers->orWhere(['city_name' => request('city_name')]);

 		if(request('state_code')) 
 			$customers->orWhere(['state_code' => request('state_code')]);

 		if(request('postal_code')) 
 			$customers->orWhere(['postal_code' => request('postal_code')]);

 		return $customers->get();
	}

	public static function searchBifrost()
	{	
		$payload = ['ClientKey' => config('app.bifrost_client_key')];
		$payload['managerVersion'] = 2;

		if(request('account_number'))
			$payload['AccountNumber'] = request('account_number');

		if(request('customer_name'))
			$payload['CustomerName'] = request('customer_name');

		if(request('meter_number'))
			$payload['MeterNumber'] = request('meter_number');

		if(request('premise_number'))
			$payload['PremiseNumber'] = request('premise_number');

		if(request('phone_number'))
			$payload['PhoneNumber'] = request('phone_number');

		if(request('house_number'))
			$payload['HouseNumber'] = request('house_number');

		if(request('street_name'))
			$payload['StreetName'] = request('street_name');

		if(request('city_name'))
			$payload['CityName'] = request('city_name');

		if(request('state_code'))
			$payload['StateCode'] = request('state_code');

		if(request('postal_code'))
			$payload['PostalCode'] = request('postal_code');

		return BifrostApi::post('Api/BSI/Account/Search', $payload);
	}

	public static function mapBifrostCustomerToDBCustomer($bifrostCustomer = null)
	{
		if($bifrostCustomer == null) return null;

		return [

			'account_id'				=> $bifrostCustomer->Id ?? '',
			'client_key'				=> $bifrostCustomer->ClientKey ?? '',
			'account_external_id' 		=> $bifrostCustomer->AccountExternalId ?? '',
			'account_active_date'		=> $bifrostCustomer->AccountActiveDate ?? '',
			'account_type'				=> $bifrostCustomer->AccountType ?? '',
			'billing_city'				=> $bifrostCustomer->BillingCity ?? '',
			'billing_postal_code'		=> $bifrostCustomer->BillingPostalCode ?? '',
			'billing_state'				=> $bifrostCustomer->BillingState ?? '',
			'billing_street'			=> $bifrostCustomer->BillingStreet ?? '',
			'shipping_city'				=> $bifrostCustomer->ShippingCity ?? '',
			'shipping_postal_code'		=> $bifrostCustomer->ShippingPostalCode,
			'shipping_street'			=> $bifrostCustomer->ShippingStreet ?? '',
			'business_type'				=> $bifrostCustomer->BusinessType ?? '',
			'customer_class'			=> $bifrostCustomer->CustomerClass ?? '',
			'electric_premise_number'	=> $bifrostCustomer->ElectricPremiseNumber ?? '',
			'electric_annual_bill'		=> $bifrostCustomer->ElectricAnnualBill ?? '',
			'electric_annual_usage'		=> $bifrostCustomer->ElectricAnnualUsage ?? '',
			'electric_meter_number'		=> $bifrostCustomer->ElectricMeterNumber ?? '',
			'electric_rate_code'		=> $bifrostCustomer->ElectricRateCode ?? '',
			'electric_utility_name'		=> $bifrostCustomer->ElectricUtilityName ?? '',
			'flat'						=> $bifrostCustomer->Flat ?? '',
			'gas_premise_number'		=> $bifrostCustomer->GasPremiseNumber ?? '',
			'gas_annual_bill'			=> $bifrostCustomer->GasAnnualBill ?? '',
			'gas_annual_usage'			=> $bifrostCustomer->GasAnnualUsage ?? '',
			'gas_utility_name'			=> $bifrostCustomer->GasUtilityName ?? '',
			'gas_meter_number'			=> $bifrostCustomer->GasMeterNumber ?? '',
			'gas_service_class'			=> $bifrostCustomer->GasServiceClass ?? '',
			'gas_rate_code'				=> $bifrostCustomer->GasRateCode ?? '',
			'gas_utility_id'			=> $bifrostCustomer->GasUtilityId ?? '',
			'water_premise_number'		=> $bifrostCustomer->WaterPremiseNumber ?? '',
			'water_annual_bill'			=> $bifrostCustomer->WaterAnnualBill ?? '',
			'water_annual_usage'		=> $bifrostCustomer->WaterAnnualUsage ?? '',
			'water_meter_number'		=> $bifrostCustomer->WaterMeterNumber ?? '',
			'water_service_class'		=> $bifrostCustomer->WaterServiceClass ?? '',
			'water_rate_code'			=> $bifrostCustomer->WaterRateCode ?? '',
			'housing_type'				=> $bifrostCustomer->HousingType ?? '',
			'low_income_referral'		=> $bifrostCustomer->LowIncomeReferral ?? '',
			'utility_id'				=> $bifrostCustomer->UtilityId ?? '',
			'program'					=> $bifrostCustomer->Program ?? '',
			'client_account_external_id'=> $bifrostCustomer->ClientAccountExternalId ?? '',
			'name'						=> $bifrostCustomer->Name ?? '',
			'email'						=> $bifrostCustomer->Email ?? '',
			'phone'						=> $bifrostCustomer->Phone ?? '',
			'home_phone'				=> $bifrostCustomer->HomePhone ?? '',
			'external_id'				=> $bifrostCustomer->ExternalId ?? '',
			'record_type'				=> $bifrostCustomer->RecordType ?? '',
			'energy_type_class'			=> $bifrostCustomer->EnergyTypeClass ?? '',
			'energy_type_class_label'	=> $bifrostCustomer->EnergyTypeClassLabel ?? '',
			'water_heat_source'			=> $bifrostCustomer->WaterHeatSource ?? '',
			'parent_id'					=> $bifrostCustomer->ParentId ?? '',
			'square_footage'			=> $bifrostCustomer->SquareFootage ?? '',
			'custom_data_value_1'		=> $bifrostCustomer->CustomDataValue1 ?? '',
			'custom_data_value_2'		=> $bifrostCustomer->CustomDataValue2 ?? '',
			'custom_data_value_3'		=> $bifrostCustomer->CustomDataValue3 ?? '',
			'custom_data_value_4'		=> $bifrostCustomer->CustomDataValue4 ?? '',
			'custom_data_value_5'		=> $bifrostCustomer->CustomDataValue5 ?? '',
			'traits_disability'			=> json_encode($bifrostCustomer->TraitsDisability),
			'traits_language'			=> json_encode($bifrostCustomer->TraitsLanguage),
			'traits_site'				=> json_encode($bifrostCustomer->TraitsSite),
			'traits'					=> json_encode($bifrostCustomer->Traits),
			'is_active'					=> $bifrostCustomer->IsActive ?? false,
			'parent_account_name'		=> $bifrostCustomer->ParentAccountName ?? '',
			'master_file_id'			=> $bifrostCustomer->MasterFileId ?? ''
		];
	}
}
