<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden    = ['updated_at', 'expires_at', 'password'];
    protected $guarded   = ['updated_at'];

    protected $casts = [ 'email_verified_at' => 'datetime'];
}
