<?php

namespace App;

use App\AccessToken;

class BifrostApi
{	
    public static function postNoAuth($endpoint = '', $payload = [])
    {
    	try{

    		$client = new \GuzzleHttp\Client();

    		$request = $client->post(config('app.bifrost_url').'/'.$endpoint, [

    			'headers' => [

    				'Content-Type' => 'application/x-www-form-urlencoded'
    			],

    			'form_params' => $payload
    		]);

    		$data = json_decode($request->getBody()->getContents(), true);

    		return ['status' => true, 'data' => $data ];

    	}catch(Exception $e){

    		return ['status' => false, 'data' => $e->getMessage()];
    	}
    }

    public static function post($endpoint = '', $payload = [])
    {
    	try{

    		$client = new \GuzzleHttp\Client();

    		$accessToken = AccessToken::getToken();

    		if(!$accessToken) return ['status' => false, 'data' => 'Could not refresh Access Token'];

    		$request = $client->post(config('app.bifrost_url').'/'.$endpoint, [

    			'headers' => [

    				'Content-Type' 		=> 'application/json',
    				'Authorization'		=> 'Bearer '.$accessToken->access_token
    			],

    			\GuzzleHttp\RequestOptions::JSON => $payload
    		]);

    		$data = json_decode($request->getBody()->getContents(), true);

    		return ['status' => true, 'data' => $data ];

    	}catch(Exception $e){

    		return ['status' => false, 'data' => $e->getMessage()];
    	
    	}catch(\GuzzleHttp\Exception\ClientException $e){

    		return ['status' => false, 'data' => $e->getMessage()];
    	}
    }

    public static function get($endpoint = '')
    {
    	try{

    		$client = new \GuzzleHttp\Client();

    		$accessToken = AccessToken::getToken();

    		if(!$accessToken) return ['status' => false, 'data' => 'Could not refresh Access Token'];

    		$request = $client->get(config('app.bifrost_url').'/'.$endpoint, [

    			'headers' => [

    				'Content-Type' 		=> 'application/json',
    				'Authorization'		=> 'Bearer '.$accessToken->access_token
    			]
    		]);

    		$data = json_decode($request->getBody()->getContents(), true);

    		return ['status' => true, 'data' => $data ];

    	}catch(Exception $e){

    		return ['status' => false, 'data' => $e->getMessage()];
    	}
    }
}
