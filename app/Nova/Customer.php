<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Customer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Customer';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->hideFromIndex(),
            Text::make('Account ID'),
            Text::make('Client Key')->sortable()->hideFromIndex(),
            Text::make('Account External ID')->hideFromIndex(),
            Text::make('Account Active Date')->hideFromIndex(),
            Text::make('Account Type')->hideFromIndex(),
            Text::make('Billing City')->hideFromIndex(),
            Text::make('Billing Postal Code')->hideFromIndex(),
            Text::make('Billing State')->hideFromIndex(),
            Text::make('Billing Street')->hideFromIndex(),
            Text::make('Business Type')->hideFromIndex(),
            Text::make('Customer Class')->hideFromIndex(),
            Text::make('Electric Annual Bill')->hideFromIndex(),
            Text::make('Electric Annual Usage')->hideFromIndex(),
            Text::make('Electric Meter Number')->hideFromIndex(),
            Text::make('Electric Rate Code')->hideFromIndex(),
            Text::make('Electric Utility Name')->hideFromIndex(),
            Text::make('Flat')->hideFromIndex(),
            Text::make('Gas Premise Number')->hideFromIndex(),
            Text::make('Gas Annual Bill')->hideFromIndex(),
            Text::make('Gas Annual Usage')->hideFromIndex(),
            Text::make('Gas Utility Name')->hideFromIndex(),
            Text::make('Gas Meter Number')->hideFromIndex(),
            Text::make('Gas Service Class')->hideFromIndex(),
            Text::make('Gas Rate Code')->hideFromIndex(),
            Text::make('Gas Utility ID')->hideFromIndex(),
            Text::make('Water Premise Number')->hideFromIndex(),
            Text::make('Water Annual Bill')->hideFromIndex(),
            Text::make('Water Annual Usage')->hideFromIndex(),
            Text::make('Water Utility Name')->hideFromIndex(),
            Text::make('Water Meter Number')->hideFromIndex(),
            Text::make('Water Service Class')->hideFromIndex(),
            Text::make('Water Rate Code')->hideFromIndex(),
            Text::make('Housing Type')->hideFromIndex(),
            Text::make('Low Income Referral')->hideFromIndex(),
            Text::make('Utility ID')->hideFromIndex(),
            Text::make('Program')->hideFromIndex(),
            Text::make('Client Account External Id')->hideFromIndex(),
            Text::make('Name')->sortable(),
            Text::make('Email')->sortable(),
            Text::make('Phone')->sortable(),
            Text::make('Home Phone')->hideFromIndex(),
            Text::make('External ID')->hideFromIndex(),
            Text::make('Record Type')->hideFromIndex(),
            Text::make('Energy Type Class')->hideFromIndex(),
            Text::make('Energy Type Class Label')->hideFromIndex(),
            Text::make('Water Heat Source')->hideFromIndex(),
            Text::make('Parent ID')->hideFromIndex(),

            Text::make('Is active')->hideFromIndex(),
            Text::make('Parent Account Name')->hideFromIndex(),
            Text::make('Master File ID'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
