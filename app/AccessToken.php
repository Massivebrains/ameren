<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BifrostApi;
use Carbon\Carbon;

class AccessToken extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];

	protected $casts = [ 'expires_at' => 'datetime'];

	public static function getToken()
	{
		$accessToken = self::first();

    	if($accessToken && $accessToken->expires_at > date('Y-m-d H:i:s'))
    		return $accessToken;

    	$payload = [

    		'username'		=> 'ICAST',
    		'password'		=> 'TDvsXNArl2qVxRYa',
    		'grant_type'	=> 'password'
    	];

    	$response = BifrostApi::postNoAuth('token', $payload);

    	if($response['status'] == false) return null;

    	$data = $response['data'];

    	$accessToken = AccessToken::updateOrCreate(['id' => 1], [

    		'access_token'	=> $data['access_token'],
    		'token_type'	=> $data['token_type'],
    		'expires_at'	=> Carbon::now()->addSeconds($data['expires_in'])->format('Y-m-d H:i:s')
    	]);

    	return $accessToken;
	}
}
